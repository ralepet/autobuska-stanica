import React from 'react';
import ReactDOM from 'react-dom';
import { Navigate, Route, Link, HashRouter as Router, Routes } from 'react-router-dom';
import { Navbar, Nav, Container, Button } from 'react-bootstrap';
import Home from './components/Home';
import Login from './components/authorization/Login'

import Linije from './components/linije/Linije';
import KreirajLiniju from './components/linije/KreirajLiniju';
import IzmeniLiniju from './components/linije/IzmeniLiniju';

import NotFound from './components/NotFound';
import { logout } from './services/auth';


class App extends React.Component {

    render() {
        const jwt = window.localStorage.getItem("jwt")
        if(jwt){
            return (
                <div>
                    <Router>
                        <Navbar expand bg="dark" variant="dark">
                            <Navbar.Brand as={Link} to="/">
                                JWD
                            </Navbar.Brand>
                            <Nav>
                                <Nav.Link as={Link} to="/linije">
                                    Linije
                                </Nav.Link>
                            </Nav>
                            <Button onClick={() => logout()}>Logout</Button>
                        </Navbar>
                        <Container style={{ paddingTop: "10px" }}>
                            <Routes>
                                <Route path="/" element={<Home />} />
                                <Route path="/linije" element={<Linije />} />
                                {window.localStorage.getItem("role") === "ROLE_ADMIN" ? <Route path="/linije/kreiraj" element={<KreirajLiniju />} /> 
                                : <Route path="/linije/kreiraj" element={<Navigate replace to="/linije" />} /> }
                                {window.localStorage.getItem("role") === "ROLE_ADMIN" ? <Route path="/linije/izmeni/:id" element={<IzmeniLiniju />} /> 
                                : <Route path="/linije/izmeni" element={<Navigate replace to="/linije" />} /> }
                                <Route path="/login" element={<Navigate replace to="/Linije" />} />
                                <Route path="*" element={<NotFound />} />
                            </Routes>
                        </Container>
                    </Router>
                </div>
            );
        }else{
            return (
                <div>
                    <Router>
                        <Navbar expand bg="dark" variant="dark">
                            <Navbar.Brand as={Link} to="/">
                                JWD
                            </Navbar.Brand>
                            <Nav>
                                <Nav.Link as={Link} to="/login">
                                    Login
                                </Nav.Link>
                            </Nav>
                        </Navbar>
                        <Container style={{ paddingTop: "10px" }}>
                            <Routes>
                                <Route path="/" element={<Home />} />
                                <Route path="/login" element={<Login />} />
                                <Route path="*" element={<Navigate replace to="/login" />} />
                            </Routes>
                        </Container>
                    </Router>
                </div>
            );
        }
        
    }
};


ReactDOM.render(
    <App />,
    document.querySelector('#root')
);
