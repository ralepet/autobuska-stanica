import React from 'react';
import TestAxios from '../../apis/TestAxios';
import { Row, Col, Button, Table, Form } from 'react-bootstrap'
import './../../index.css';
import { withParams, withNavigation } from '../../routeconf'

class Linije extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            checked: false,
            linije: [], 
            pretraga: { 
                destinacija: "",
                cenaKarteDo: "",
                prevoznikId: ""
            },
            currentPage:0, 
            totalPages:0,
            prevoznici: []
        }
    }

    componentDidMount() {
        this.getLinije(0);
        this.getPrevoznici();
    }

    async getPrevoznici(){
        try{
            let result = await TestAxios.get("/prevoznici");
            let prevoznici = result.data; 
            this.setState({prevoznici: prevoznici});
            console.log("Prevoznici su uspesno dobavljeni");
        }catch(error){
            console.log(error);
            alert("Prevoznici nisu dobavljeni");
        } 
    }

    async getLinije(pageNo) {
        const config = {
            params: {
                pageNo: pageNo,
                destinacija: this.state.pretraga.destinacija,
                cenaKarteDo: this.state.pretraga.cenaKarteDo,
                prevoznik_id: this.state.pretraga.prevoznikId
            }
        }
        try {
            let result = await TestAxios.get('/linije', config);
            let total_pages = result.headers["total-pages"]
            this.setState({
                linije: result.data,
              currentPage: pageNo,
              totalPages: total_pages
            });
          } catch (error) {
            console.log(error);
          }
    }  

    idiNaKreiranje() {
        this.props.navigate('/linije/kreiraj');
    }

    idiNaIzmenu(linijaId) {
        this.props.navigate('/linije/izmeni/' + linijaId);
    }

    obrisi(linijaId) {
        TestAxios.delete('/linije/' + linijaId)
            .then(res => {
                // handle success
                console.log(res);
                alert('Linija je uspesno obrisana!');
                window.location.reload();
            })
            .catch(error => {
                // handle error
                console.log(error);
                alert('Error occured please try again!');
            });
    }

    idiNaPrethodnuStranicu(){
        this.getLinije(this.state.currentPage-1) 
    }

    idiNaSledecuStranicu(){ //
        this.getLinije(this.state.currentPage+1) 
    }

    onInputChange(event) {
        const name = event.target.name;
        const value = event.target.value

        let pretraga = this.state.pretraga;
        pretraga[name] = value;

        this.setState({ pretraga: pretraga })
    }

    async rezervisi(idLinije, brojMesta) {

        if(brojMesta >= 1){
            try {
                let rezervacijaDto = {
                    linijaId: idLinije
                      
                  }
                await TestAxios.post("/rezervacije", rezervacijaDto);
                alert('Karta je uspesno rezervisana!');
                window.location.reload();
              } catch (error) {
                // handle error
                console.log(error);
                alert('Karta nije uspesno rezervisana!');
              }
        } else{
            alert('Broj mesta mora biti veci od 0!');
        }

    }
    
    renderLinija() {
        return this.state.linije.map((linija, index) => {
            return (
                <tr key={linija.id}>
                    <td>{linija.prevoznikNaziv}</td>
                    <td>{linija.destinacija}</td>
                    <td>{linija.brojMesta}</td>
                    <td>{linija.vremePolaska}</td>
                    <td>{linija.cena} din</td>
                    {window.localStorage.getItem("role") === "ROLE_ADMIN" ?
                        <td><Button variant="danger" onClick={() => this.obrisi(linija.id)}>Obrisi</Button></td>
                        : <td></td>}

                    {window.localStorage.getItem("role") === "ROLE_ADMIN" ?
                        <td><Button variant="warning" onClick={() => this.idiNaIzmenu(linija.id)}>Izmeni</Button></td>
                        : null}

                    {window.localStorage.getItem("role") === "ROLE_KORISNIK" ?
                        <td><Button variant="primary" onClick={() => this.rezervisi(linija.id, linija.brojMesta)}>Rezervisi</Button></td>
                        : null}
                </tr>
            )
        })
    }


    render() {
        return (
            <Col>
                <Row><h1>Linije</h1></Row>
                <>
                    <Form.Check onChange={() => this.setState({ checked: !this.state.checked })} label="Sakrij pretragu" />
                </>
                <Form  hidden={this.state.checked} style={{ width: "100%" }}>                     
                    <Form.Group>
                        <Form.Label>Destinacija</Form.Label> 
                        <Form.Control name="destinacija" type="text"  onChange={(e) => this.onInputChange(e)}></Form.Control>
                    </Form.Group>
                    <Form.Label>Prevoznik</Form.Label>  
                    <Form.Control as="select"  name="prevoznikId" onChange={e => this.onInputChange(e)}>
                        <option value="">Izaberi prevoznika</option> 
                        {
                            this.state.prevoznici.map((prevoznik) => {
                                return (
                                    <option key={prevoznik.id} value={prevoznik.id}>{prevoznik.naziv}</option>
                                )
                            })
                        }
                    </Form.Control><br/> 
                    <Form.Group>
                        <Form.Label>Maksimalna cena</Form.Label> 
                        <Form.Control name="cenaKarteDo" type="text"  onChange={(e) => this.onInputChange(e)}></Form.Control>
                    </Form.Group>
                    <Button onClick={() => this.getLinije(0)}>Pretraga</Button>
                </Form>
                <br /><br />
                <Row>
                    {window.localStorage.getItem("role") === "ROLE_ADMIN" ?
                        <Button variant="success" onClick={() => this.idiNaKreiranje()}>Dodaj liniju</Button> : null
                    }
                    <br /><br />
                <Button variant="info" disabled={this.state.currentPage===0} onClick={()=>this.idiNaPrethodnuStranicu()}>Prethodna</Button>
                <Button variant="info" disabled={this.state.currentPage===this.state.totalPages-1} onClick={()=>this.idiNaSledecuStranicu()}>Sledeca</Button>
                </Row>
                <Row>
                    <Table className="table table-striped" style={{ marginTop: 5 }} >
                        <thead className="thead-dark">
                            <tr>
                                <th>Naziv prevoznika</th>
                                <th>Destinacija</th>  
                                <th>Broj mesta</th>
                                <th>Vreme polaska</th> 
                                <th>Cena karte</th> 
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.renderLinija()}
                        </tbody>
                    </Table>
                </Row>
            </Col>
        );
    }
}

export default withNavigation(withParams(Linije));