import React from "react";
import { Form, Button, Row, Col } from "react-bootstrap";
import TestAxios from '../../apis/TestAxios';
import { withNavigation } from "../../routeconf";

class KreirajLiniju extends React.Component {

    constructor(props){
        super(props);

        let linija = {
            idPrevoznika: "",
            brojMesta: "",
            cenaKarte: "" ,
            destinacija: "", 
            vremePolaska: "",
        }

        this.state = {
           
            linija: linija,
            prevoznici: []
        }

    }

    componentDidMount(){
        this.getPrevoznici();
    }

    async getPrevoznici(){
        try{
            let result = await TestAxios.get("/prevoznici");
            let prevoznici = result.data;
            this.setState({prevoznici: prevoznici});
            console.log("uspesno su dobavljeni prevoznici");
            //alert("uspesno su dobavljeni prevoznici");
        }catch(error){
            console.log(error);
            alert("Prevoznici nisu uspesno dobavljeni");
        }
    }

    async dodajLiniju(e){ 
        e.preventDefault();

        try{
            let linija = this.state.linija;
            console.log(linija)

            let linijaDto = {

              prevoznikId: linija.idPrevoznika,
              brojMesta: linija.brojMesta,
              cena: linija.cenaKarte,
              destinacija: linija.destinacija,
              vremePolaska: linija.vremePolaska,
                
            }
            await TestAxios.post("/linije", linijaDto);
            //let response =  await TestAxios.post("/linije", linijaDto);
            this.props.navigate("/linije");
        }catch(error){
            alert("Linija nije kreirana");
        }
    }

    valueInputChanged(e) {
        let input = e.target;
    
        let name = input.name;
        let value = input.value;
    
        let linija = this.state.linija;
        linija[name] = value;
    
        this.setState({ linija: linija });
      }
    

    render() {
        return (
          <>
            <Row>
              <Col></Col>
              <Col xs="12" sm="10" md="8">
                <h1>Dodaj novu liniju</h1> 
                <Form>
                  <Form.Label>Broj mesta</Form.Label>
                  <Form.Control
                    type="text"
                    name="brojMesta"
                    onChange={(e) => this.valueInputChanged(e)}
                    />
                  <Form.Label>Cena karte</Form.Label>
                  <Form.Control
                    type="text"
                    name="cenaKarte"
                    onChange={(e) => this.valueInputChanged(e)}
                    />
                    <Form.Label>Destinacija</Form.Label>
                    <Form.Control
                    type="text"
                    name="destinacija"
                    onChange={(e) => this.valueInputChanged(e)}
                     />
                      <Form.Label>Vreme polaska</Form.Label>
                    <Form.Control
                    type="text"
                    name="vremePolaska"
                    onChange={(e) => this.valueInputChanged(e)}
                     />
                    <Form.Label>Prevoznik</Form.Label>   
                    <Form.Control as="select" name="idPrevoznika" onChange={event => this.valueInputChanged(event)}>
                        <option>Izaberi prevoznika</option> 
                        {
                            this.state.prevoznici.map((prevoznik) => {
                                return (
                                    <option key={prevoznik.id} value={prevoznik.id}>{prevoznik.naziv}</option>
                                )
                            })
                        }
                    </Form.Control><br/> 
    
                  <Button style={{ marginTop: "25px" }} onClick={(event)=>{this.dodajLiniju(event);}}>
                    Dodaj liniju
                  </Button>
                </Form>
              </Col>
              <Col></Col>
            </Row>                           
            
          </>
        );
      }
}

export default withNavigation(KreirajLiniju);