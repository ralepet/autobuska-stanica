import React from "react";
import { Form, Button, Row, Col } from "react-bootstrap";
import TestAxios from '../../apis/TestAxios';
import {withParams, withNavigation} from '../../routeconf';

class IzmeniLiniju extends React.Component {

  constructor(props){
    super(props);

    let linija = {
        idPrevoznika: "",
        brojMesta: "",
        cena: "" ,
        destinacija: "", 
        vremePolaska: "",
    }

    this.state = {
        idLinije :-1,
        linija: linija,
        prevoznici: []
    }

}

componentDidMount(){
    this.getPrevoznici();
    this.getLinijaById(this.props.params.id);
}

getLinijaById(linijaId) {
  TestAxios.get('/linije/' + linijaId)
  .then(res => {
      // handle success
      console.log(res.data);
      //let linija = this.state.linija;
      let linija = {}
      linija.idPrevoznika = res.data.prevoznikId
      linija.brojMesta = res.data.brojMesta
      linija.cena =  res.data.cena
      linija.destinacija = res.data.destinacija
      linija.vremePolaska = res.data.vremePolaska
      console.log(linija)

      this.setState({idLinije: res.data.id, linija: linija});
      console.log(this.state.linija)
      
  })
  .catch(error => {
      // handle error
      console.log(error);
      alert('Error occured please try again!');
   });
}

async getPrevoznici(){
    try{
        let result = await TestAxios.get("/prevoznici");
        let prevoznici = result.data;
        this.setState({prevoznici: prevoznici});
        console.log("uspesno su dobavljeni prevoznici");
        //alert("uspesno su dobavljeni prevoznici");
    }catch(error){
        console.log(error);
        alert("Prevoznici nisu uspesno dobavljeni");
    }
}

async izmeni() {
  try{
    var params = {
      'id': this.state.idLinije,
      'prevoznikId': this.state.linija.idPrevoznika,
      'brojMesta': this.state.linija.brojMesta, 
      'cena' : this.state.linija.cena,
      'destinacija': this.state.linija.destinacija, 
      'vremePolaska': this.state.linija.vremePolaska
    };
    await TestAxios.put('/linije/' + this.state.idLinije, params);

      alert('Linija je uspesno izmenjena!');
      this.props.navigate('/linije');
  }catch(error){
    console.log(error);
    alert("Linija nije izmenjena");
  }

}

valueInputChanged(e) {
    let input = e.target;

    let name = input.name;
    let value = input.value;

    let linija = this.state.linija;
    linija[name] = value;

    this.setState({ linija: linija });
  }


render() {
    return (
      <>
        <Row>
          <Col></Col>
          <Col xs="12" sm="10" md="8">
            <h1>Izmeni liniju</h1> 
            <Form>
              <Form.Label>Broj mesta</Form.Label>
              <Form.Control
                type="text"
                name="brojMesta"
                value={this.state.linija.brojMesta}
                onChange={(e) => this.valueInputChanged(e)}
                />
              <Form.Label>Cena karte</Form.Label>
              <Form.Control
                type="text"
                name="cenaKarte"
                value={this.state.linija.cena}
                onChange={(e) => this.valueInputChanged(e)}
                />
                <Form.Label>Destinacija</Form.Label>
                <Form.Control
                type="text"
                name="destinacija"
                value={this.state.linija.destinacija}
                onChange={(e) => this.valueInputChanged(e)}
                 />
                  <Form.Label>Vreme polaska</Form.Label>
                <Form.Control
                type="text"
                name="vremePolaska"
                value={this.state.linija.vremePolaska}
                onChange={(e) => this.valueInputChanged(e)}
                 />
                <Form.Label>Prevoznik</Form.Label>   
                <Form.Control as="select" name="idPrevoznika" onChange={event => this.valueInputChanged(event)}>
                    <option>Izaberi prevoznika</option> 
                    {
                        this.state.prevoznici.map((prevoznik) => {
                            return (
                                <option key={prevoznik.id} value={prevoznik.id} selected={prevoznik.id === this.state.linija.idPrevoznika}>{prevoznik.naziv}</option>
                            )
                        })
                    }
                </Form.Control><br/> 

              <Button style={{ marginTop: "25px" }} onClick={() => this.izmeni()}>
                Izmeni
              </Button>
            </Form>
          </Col>
          <Col></Col>
        </Row>                           
        
      </>
    );
  }
}

export default withNavigation(withParams(IzmeniLiniju));