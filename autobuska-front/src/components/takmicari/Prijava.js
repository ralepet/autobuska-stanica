import React from "react";
import { Form, Button, Row, Col } from "react-bootstrap";
import TestAxios from './../../apis/TestAxios';
import {withParams, withNavigation} from '../../routeconf';

class Prijava extends React.Component {

    constructor(props){
        super(props);

        let prijava = {
            disciplina: ""
        }

        this.state = {
            prijava: prijava, idTakmicara: "", imePrezimeTakmicara: ""
        }

    }

    componentDidMount() {
        this.getTakmicarById(this.props.params.id);
     }
 
     getTakmicarById(takmicarId) {
        TestAxios.get('/takmicari/' + takmicarId)
         .then(res => {
             // handle success
             console.log(res);
             this.setState({idTakmicara: res.data.id, imePrezimeTakmicara: res.data.imePrezime});
         })
         .catch(error => {
             // handle error
             console.log(error);
             alert('Error occured please try again!');
          });
     }

    async prijaviTakmicara(e){ 
        e.preventDefault();

        try{
            let prijava = this.state.prijava;
            console.log(prijava)

            let prijavaDto = {

            idTakmicara: this.state.idTakmicara,
            disciplina: this.state.prijava.disciplina
                
            }
            await TestAxios.post("/prijave", prijavaDto);
            //let response = await TestAxios.post("/prijave", takmicarDto);
            this.props.navigate("/takmicari");
        }catch(error){
            alert("Prijava nije uspesna");
        }
    }

    valueInputChanged(e) {
        let input = e.target;
    
        let name = input.name;
        let value = input.value;
    
        let prijava = this.state.prijava;
        prijava[name] = value;
    
        this.setState({ prijava: prijava });
    }

    render() {
        return (
          <>
            <Row>
              <Col></Col>
              <Col xs="12" sm="10" md="8">
                <h1>Prijavi takmicara {this.state.imePrezimeTakmicara}</h1>
                <Form>
                  <Form.Label>Disciplina takmicara</Form.Label> 
                  <Form.Control
                    type="text"
                    placeholder="Unesite disciplinu"
                    name="disciplina"
                    onChange={(e) => this.valueInputChanged(e)}
                    />
    
                  <Button style={{ marginTop: "25px" }} onClick={(event)=>{this.prijaviTakmicara(event);}}>
                    Prijavi Takmicara
                  </Button>
                </Form>
              </Col>
              <Col></Col>
            </Row>                           
            
          </>
        );
      }


}

export default withNavigation(withParams(Prijava));