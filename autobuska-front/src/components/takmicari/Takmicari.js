import React from 'react';
import TestAxios from './../../apis/TestAxios';
import { Row, Col, Button, Table, Form } from 'react-bootstrap'
import './../../index.css';
import { withParams, withNavigation } from '../../routeconf'

class Takmicari extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            checked: false,
            takmicari: [], 
            pretraga: { 
                brojMedaljaOd: "",
                brojMedaljaDo: "",
                drzavaId: ""
            },
            currentPage:0, 
            totalPages:0,
            drzave: []
        }
    }

    componentDidMount() {
        this.getTakmicari(0);
        this.getDrzave();
    }

    async getDrzave(){
        try{
            let result = await TestAxios.get("/drzave");
            let drzave = result.data; 
            this.setState({drzave: drzave});
            console.log("Drzave su uspesno dobavljene");
        }catch(error){
            console.log(error);
            alert("Drzave nisu dobavljene");
        } 
    }

    async getTakmicari(pageNo) {
        const config = {
            params: {
                pageNo: pageNo,
                brojMedaljaOd: this.state.pretraga.brojMedaljaOd,
                brojMedaljaDo: this.state.pretraga.brojMedaljaDo,
                drzava_id: this.state.pretraga.drzavaId
            }
        }
        try {
            let result = await TestAxios.get('/takmicari', config);
            let total_pages = result.headers["total-pages"]
            this.setState({
            takmicari: result.data,
              currentPage: pageNo,
              totalPages: total_pages
            });
          } catch (error) {
            console.log(error);
          }
    }  

    idiNaKreiranje() {
        this.props.navigate('/takmicari/kreiraj');
    }

    obrisi(takmicarId) {
        TestAxios.delete('/takmicari/' + takmicarId)
            .then(res => {
                // handle success
                console.log(res);
                alert('Takmicar je uspesno obrisan!');
                window.location.reload();
            })
            .catch(error => {
                // handle error
                console.log(error);
                alert('Error occured please try again!');
            });
    }

    idiNaPrethodnuStranicu(){
        this.getTakmicari(this.state.currentPage-1) 
    }

    idiNaSledecuStranicu(){ //
        this.getTakmicari(this.state.currentPage+1) 
    }

    onInputChange(event) {
        const name = event.target.name;
        const value = event.target.value

        let pretraga = this.state.pretraga;
        pretraga[name] = value;

        this.setState({ pretraga: pretraga })
    }

    idiNaPrijavu(takmicarId) {
        this.props.navigate('/takmicari/prijava/' + takmicarId);
    }

    idiNaStatistiku() {
        this.props.navigate('/statistika/');
    }
    
    renderTakmicara() {
        return this.state.takmicari.map((takmicar, index) => {
            return (
                <tr key={takmicar.id}>
                    <td>{takmicar.imePrezime}</td>
                    <td>{takmicar.nazivOznakaDrzave}</td>
                    <td>{takmicar.datumRodjenja}</td>
                    <td>{takmicar.brojMedalja}</td>
                    {window.localStorage.getItem("role") === "ROLE_ADMIN" ?
                        <td><Button variant="danger" onClick={() => this.obrisi(takmicar.id)}>Obrisi</Button></td>
                        : <td></td>}

                    {window.localStorage.getItem("role") === "ROLE_KORISNIK" ?
                        <td><Button variant="primary" onClick={() => this.idiNaPrijavu(takmicar.id)}>Prijava</Button></td>
                        : <td></td>}
                </tr>
            )
        })
    }


    render() {
        return (
            <Col>
                <Row><h1>Takmicari</h1></Row>
                <>
                    <Form.Check onChange={() => this.setState({ checked: !this.state.checked })} label="Sakrij pretragu" />
                </>
                <Form  hidden={this.state.checked} style={{ width: "100%" }}>                     
                   
                    <Form.Label>Drzava takmicara</Form.Label> 
                    <Form.Control as="select"  name="drzavaId" onChange={e => this.onInputChange(e)}>
                        <option value="">Izaberi drzavu</option>
                        {
                            this.state.drzave.map((drzava) => {
                                return (
                                    <option key={drzava.id} value={drzava.id}>{drzava.naziv}</option>
                                )
                            })
                        }
                    </Form.Control><br/> 
                    <Form.Group>
                        <Form.Label>Broj medalja od</Form.Label> 
                        <Form.Control name="brojMedaljaOd" type="text"  onChange={(e) => this.onInputChange(e)}></Form.Control>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Broj medalja do</Form.Label> 
                        <Form.Control name="brojMedaljaDo" type="text"  onChange={(e) => this.onInputChange(e)}></Form.Control>
                    </Form.Group>
                    <Button onClick={() => this.getTakmicari(0)}>Pretraga</Button>
                </Form>
                <br /><br />
                <Row>
                    {window.localStorage.getItem("role") === "ROLE_ADMIN" ?
                        <Button variant="success" onClick={() => this.idiNaKreiranje()}>Dodaj takmicara</Button> : null
                    }
                    <br /><br />
                <Button variant="warning" onClick={() => this.idiNaStatistiku()}>Statistika</Button>
                <Button variant="info" disabled={this.state.currentPage===0} onClick={()=>this.idiNaPrethodnuStranicu()}>Prethodna</Button>
                <Button variant="info" disabled={this.state.currentPage===this.state.totalPages-1} onClick={()=>this.idiNaSledecuStranicu()}>Sledeca</Button>
                </Row>
                <Row>
                    <Table className="table table-striped" style={{ marginTop: 5 }} >
                        <thead className="thead-dark">
                            <tr>
                                <th>Ime i prezime takmicara</th>
                                <th>Drzava</th>  
                                <th>Datum rodjenja</th>
                                <th>Boj osvojenih medalja</th> 
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.renderTakmicara()}
                        </tbody>
                    </Table>
                </Row>
            </Col>
        );
    }
}

export default withNavigation(withParams(Takmicari));