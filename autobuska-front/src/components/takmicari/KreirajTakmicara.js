import React from "react";
import { Form, Button, Row, Col } from "react-bootstrap";
import TestAxios from './../../apis/TestAxios';
import { withNavigation } from "../../routeconf";

class KreirajTakmicara extends React.Component {

    constructor(props){
        super(props);

        let takmicar = {
            idDrzave: "",
            brojMedalja: "",
            imePrezime: "" ,
            nazivOznakaDrzave: "", 
            datumRodjenja: "",
        }

        this.state = {
           
            takmicar: takmicar,
            drzave: []
        }

    }

    componentDidMount(){
        this.getDrzave();
    }

    async getDrzave(){
        try{
            let result = await TestAxios.get("/drzave");
            let drzave = result.data;
            this.setState({drzave: drzave});
            console.log("uspesno su dobavljene drzave");
            //alert("uspesno su dobavljene drzave"");
        }catch(error){
            console.log(error);
            alert("Drzave nisu uspesno dobavljene");
        }
    }

    async dodajTakmicara(e){ 
        e.preventDefault();

        try{
            let takmicar = this.state.takmicar;
            console.log(takmicar)

            let takmicarDto = {

              idDrzave: takmicar.idDrzave,
              brojMedalja: takmicar.brojMedalja,
              imePrezime: takmicar.imePrezime,
              nazivOznakaDrzave: takmicar.nazivOznakaDrzave,
              datumRodjenja: takmicar.datumRodjenja,
                
            }
            await TestAxios.post("/takmicari", takmicarDto);
            //let response = await TestAxios.post("/takmicari", takmicarDto);
            this.props.navigate("/takmicari");
        }catch(error){
            alert("Takmicar nije kreiran");
        }
    }

    valueInputChanged(e) {
        let input = e.target;
    
        let name = input.name;
        let value = input.value;
    
        let takmicar = this.state.takmicar;
        takmicar[name] = value;
    
        this.setState({ takmicar: takmicar });
      }
    

    render() {
        return (
          <>
            <Row>
              <Col></Col>
              <Col xs="12" sm="10" md="8">
                <h1>Dodaj novog takmicara</h1>
                <Form>
                  <Form.Label>Ime i prezime takmicara</Form.Label>
                  <Form.Control
                    type="text"
                    placeholder="Ime i prezime takmicara"
                    name="imePrezime"
                    onChange={(e) => this.valueInputChanged(e)}
                    />
                  <Form.Label>Datum rodjenja</Form.Label>
                  <Form.Control
                    type="date"
                    name="datumRodjenja"
                    placeholder="Datum rodjenja"
                    onChange={(e) => this.valueInputChanged(e)}
                    />
                    <Form.Label>Broj osvojenih medalja</Form.Label>
                    <Form.Control
                    type="text"
                    name="brojMedalja"
                    placeholder="Broj osvojenih medalja"
                    onChange={(e) => this.valueInputChanged(e)}
                     />
                    <Form.Label>Drzava takmicara</Form.Label>   
                    <Form.Control as="select" name="idDrzave" onChange={event => this.valueInputChanged(event)}>
                        <option>Izaberi drzavu takmicara</option>
                        {
                            this.state.drzave.map((drzava) => {
                                return (
                                    <option key={drzava.id} value={drzava.id}>{drzava.naziv}, {drzava.oznaka}</option>
                                )
                            })
                        }
                    </Form.Control><br/> 
    
                  <Button style={{ marginTop: "25px" }} onClick={(event)=>{this.dodajTakmicara(event);}}>
                    Dodaj Takmicara
                  </Button>
                </Form>
              </Col>
              <Col></Col>
            </Row>                           
            
          </>
        );
      }
}

export default withNavigation(KreirajTakmicara);