package com.ftninformatika.jwd.test.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.RezervacijaDto;
import com.ftninformatika.jwd.test.model.Rezervacija;
import com.ftninformatika.jwd.test.service.LinijaService;
import com.ftninformatika.jwd.test.service.RezervacijaService;

@Component
public class DtoToRezervacija implements Converter<RezervacijaDto, Rezervacija>{
	
	@Autowired
	private LinijaService linijaService;
	
	@Autowired
	private RezervacijaService rezervacijaService;

	@Override
	public Rezervacija convert(RezervacijaDto dto) {

		Rezervacija entity; 
		if (dto.getId() == null) {
			entity = new Rezervacija();
		} else {
			entity = rezervacijaService.findOneById(dto.getId());
		}
		
		entity.setId(dto.getId());
		entity.setLinija(linijaService.findOneById(dto.getLinijaId()));
		
		return entity;
	}

}
