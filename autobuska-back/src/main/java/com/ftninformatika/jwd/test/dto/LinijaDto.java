package com.ftninformatika.jwd.test.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class LinijaDto {
	
	private Long id;
	
	@Positive(message = "Id mora biti pozitivan broj.")
	@NotNull
	private Long prevoznikId;

	private String prevoznikNaziv; 
	
	private Integer brojMesta;

	@NotNull(message = "Nije zadata cena karte.")
	@Positive(message = "Cena karte nije pozitivan broj.")
	private double cena;

	@NotBlank(message = "Destinacija nije zadata.")
	private String destinacija;
	
	private String vremePolaska;

	public LinijaDto() {
		super();
	}

	public Long getPrevoznikId() {
		return prevoznikId;
	}

	public void setPrevoznikId(Long prevoznikId) {
		this.prevoznikId = prevoznikId;
	}

	public String getPrevoznikNaziv() {
		return prevoznikNaziv;
	}

	public void setPrevoznikNaziv(String prevoznikNaziv) {
		this.prevoznikNaziv = prevoznikNaziv;
	}

	public double getCena() {
		return cena;
	}

	public void setCena(double cena) {
		this.cena = cena;
	}

	public String getDestinacija() {
		return destinacija;
	}

	public void setDestinacija(String destinacija) {
		this.destinacija = destinacija;
	}

	public String getVremePolaska() {
		return vremePolaska;
	}

	public void setVremePolaska(String vremePolaska) {
		this.vremePolaska = vremePolaska;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getBrojMesta() {
		return brojMesta;
	}

	public void setBrojMesta(Integer brojMesta) {
		this.brojMesta = brojMesta;
	}
	
	
}
