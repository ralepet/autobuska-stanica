package com.ftninformatika.jwd.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.LinijaDto;
import com.ftninformatika.jwd.test.model.Linija;

@Component
public class LinijaToDto  implements Converter<Linija, LinijaDto>{

	@Override
	public LinijaDto convert(Linija linija) {
		
		LinijaDto dto = new LinijaDto();
		dto.setId(linija.getId());
		dto.setBrojMesta(linija.getBrojMesta());
		dto.setCena(linija.getCena());
		dto.setDestinacija(linija.getDestinacija());
		dto.setPrevoznikId(linija.getPrevoznik().getId());
		dto.setPrevoznikNaziv(linija.getPrevoznik().getNaziv());
		dto.setVremePolaska(linija.getVremePolaska().toString());
		return dto;
	}
	
	
	public List<LinijaDto> convert(List<Linija>list){
		List<LinijaDto>dto = new ArrayList<>();
		for(Linija linija : list) {  
			dto.add(convert(linija));
		}
		return dto;
	}

}
