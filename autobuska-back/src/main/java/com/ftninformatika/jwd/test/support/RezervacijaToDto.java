package com.ftninformatika.jwd.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.RezervacijaDto;
import com.ftninformatika.jwd.test.model.Rezervacija;

@Component
public class RezervacijaToDto implements Converter<Rezervacija, RezervacijaDto>{

	@Override
	public RezervacijaDto convert(Rezervacija rezervacija) {
		
		RezervacijaDto dto = new RezervacijaDto();
		dto.setId(rezervacija.getId());
		dto.setLinijaId(rezervacija.getLinija().getId());

		return dto;
	}
	
	public List<RezervacijaDto> convert(List<Rezervacija>list){
		List<RezervacijaDto>dto = new ArrayList<>();
		for(Rezervacija rezervacija : list) {   
			dto.add(convert(rezervacija));
		}
		return dto;
	}

}
