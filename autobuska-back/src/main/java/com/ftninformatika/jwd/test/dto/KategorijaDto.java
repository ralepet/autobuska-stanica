package com.ftninformatika.jwd.test.dto;

public class KategorijaDto {
	
	Long id;
	
	String naziv;

	public KategorijaDto() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	
	

}
