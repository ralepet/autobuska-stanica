package com.ftninformatika.jwd.test.model;

import java.time.LocalTime;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Linija {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable = false)
	private Integer brojMesta;
	
	@Column
	private Double cena;
	
	@Column(nullable = false)
	private LocalTime vremePolaska;
	
	@Column(nullable = false)
	private String destinacija;
	
	@ManyToOne
	private Prevoznik prevoznik;
	
	@OneToMany(mappedBy = "linija", cascade = CascadeType.ALL)
	List<Rezervacija> rezervacije;
	

	public Linija() {
		super();
	}

	public Linija(Long id, Integer brojMesta, Double cena, LocalTime vremePolaska, String destinacija,
			Prevoznik prevoznik) {
		super();
		this.id = id;
		this.brojMesta = brojMesta;
		this.cena = cena;
		this.vremePolaska = vremePolaska;
		this.destinacija = destinacija;
		this.prevoznik = prevoznik;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getBrojMesta() {
		return brojMesta;
	}

	public void setBrojMesta(Integer brojMesta) {
		this.brojMesta = brojMesta;
	}

	public Double getCena() {
		return cena;
	}

	public void setCena(Double cena) {
		this.cena = cena;
	}


	public LocalTime getVremePolaska() {
		return vremePolaska;
	}


	public void setVremePolaska(LocalTime vremePolaska) {
		this.vremePolaska = vremePolaska;
	}


	public List<Rezervacija> getRezervacije() {
		return rezervacije;
	}


	public void setRezervacije(List<Rezervacija> rezervacije) {
		this.rezervacije = rezervacije;
	}


	public String getDestinacija() {
		return destinacija;
	}

	public void setDestinacija(String destinacija) {
		this.destinacija = destinacija;
	}

	public Prevoznik getPrevoznik() {
		return prevoznik;
	}

	public void setPrevoznik(Prevoznik prevoznik) {
		this.prevoznik = prevoznik;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Linija other = (Linija) obj;
		return Objects.equals(id, other.id);
	}
	
	

}
