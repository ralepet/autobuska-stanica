package com.ftninformatika.jwd.test.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.test.dto.PrevoznikDto;
import com.ftninformatika.jwd.test.model.Prevoznik;
import com.ftninformatika.jwd.test.service.PrevoznikService;
import com.ftninformatika.jwd.test.support.DtoToPrevoznik;
import com.ftninformatika.jwd.test.support.PrevoznikToDto;

@RestController
@RequestMapping(value = "/api/prevoznici", produces = MediaType.APPLICATION_JSON_VALUE)
public class PrevoznikController {
	
	@Autowired
	private PrevoznikService prevoznikService;
	
	@Autowired
	private PrevoznikToDto toDto;
	
	@Autowired
	private DtoToPrevoznik toPrevoznik;
	 
	@GetMapping
	public ResponseEntity<List<PrevoznikDto>> getAll() {

		List<Prevoznik> list = prevoznikService.findAll();

		return new ResponseEntity<>(toDto.convert(list), HttpStatus.OK);
	}
	
	//@PreAuthorize("hasAnyAuthority('ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PrevoznikDto> create(@Valid @RequestBody PrevoznikDto prevoznikDto){
        Prevoznik prevoznik = toPrevoznik.convert(prevoznikDto);

        Prevoznik sacuvaniPrevoznik = prevoznikService.save(prevoznik);

        return new ResponseEntity<>(toDto.convert(sacuvaniPrevoznik), HttpStatus.CREATED);
    }
	
	@ExceptionHandler(value = DataIntegrityViolationException.class)
    public ResponseEntity<Void> handle() {
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
