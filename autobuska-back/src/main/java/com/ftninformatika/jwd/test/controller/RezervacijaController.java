package com.ftninformatika.jwd.test.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.test.dto.RezervacijaDto;
import com.ftninformatika.jwd.test.model.Rezervacija;
import com.ftninformatika.jwd.test.service.LinijaService;
import com.ftninformatika.jwd.test.service.RezervacijaService;
import com.ftninformatika.jwd.test.support.DtoToRezervacija;
import com.ftninformatika.jwd.test.support.RezervacijaToDto;



@RestController
@RequestMapping(value = "/api/rezervacije", produces = MediaType.APPLICATION_JSON_VALUE)
public class RezervacijaController {
	
	@Autowired
	RezervacijaService rezervacijaService;
	
	@Autowired
	LinijaService linijaService;
	
	@Autowired
	private DtoToRezervacija toRezervacija;
	
	@Autowired
	private RezervacijaToDto toDto;
	
	//@PreAuthorize("hasAnyAuthority('ADMIN')")
	@GetMapping
	public ResponseEntity<List<RezervacijaDto>> getAll() {

		List<Rezervacija> list = rezervacijaService.findAll();

		return new ResponseEntity<>(toDto.convert(list), HttpStatus.OK);
	}
	
//	@PreAuthorize("hasAnyAuthority('KORISNIK')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE) 
	public ResponseEntity<RezervacijaDto> create(@RequestBody RezervacijaDto rezervacijaDto) {
		Rezervacija rezervacija = toRezervacija.convert(rezervacijaDto); 
		
		// ukoliko linija ima vrednost null, ili ukoliko je broj slobodnih mesta na liniji madnji od 1, vracamo Bad request
		if (rezervacija.getLinija() == null || rezervacija.getLinija().getBrojMesta() < 1) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} 
		
		linijaService.oduzmiBrojMesta(rezervacija.getLinija().getId()); // umanjujemo za 1

		Rezervacija save = rezervacijaService.save(rezervacija);

		return new ResponseEntity<>(toDto.convert(save), HttpStatus.CREATED);
	}
	
	@ExceptionHandler(value = DataIntegrityViolationException.class)
    public ResponseEntity<Void> handle() {
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
