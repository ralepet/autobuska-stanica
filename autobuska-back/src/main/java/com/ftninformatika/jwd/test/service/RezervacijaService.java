package com.ftninformatika.jwd.test.service;

import java.util.List;

import com.ftninformatika.jwd.test.model.Rezervacija;

public interface RezervacijaService {
	Rezervacija findOneById(Long id);

	List<Rezervacija> findAll();
	
	Rezervacija save (Rezervacija rezervacija);
	
	Rezervacija update (Rezervacija rezervacija);
}
