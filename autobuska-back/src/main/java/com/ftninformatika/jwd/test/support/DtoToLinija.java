package com.ftninformatika.jwd.test.support;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.LinijaDto;
import com.ftninformatika.jwd.test.model.Linija;
import com.ftninformatika.jwd.test.service.LinijaService;
import com.ftninformatika.jwd.test.service.PrevoznikService;

@Component
public class DtoToLinija  implements Converter<LinijaDto, Linija>{
	
	@Autowired
	private LinijaService linijaService;
	
	@Autowired
	private PrevoznikService prevoznikService; 

	@Override
	public Linija convert(LinijaDto dto) {

		Linija entity; 
		if (dto.getId() == null) {
			entity = new Linija();
		} else {
			entity = linijaService.findOneById(dto.getId());
		}
		
		entity.setCena(dto.getCena());
		entity.setDestinacija(dto.getDestinacija());
		entity.setVremePolaska(getLocalTime(dto.getVremePolaska()));
		entity.setBrojMesta(dto.getBrojMesta());
		entity.setPrevoznik(prevoznikService.findOneById(dto.getPrevoznikId()));
		
		return entity;
	}
	
	 private LocalTime getLocalTime(String dateTime) throws DateTimeParseException {
	        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
	        return LocalTime.parse(dateTime, formatter);
	 }

}
