package com.ftninformatika.jwd.test.service;

import java.util.List;

import org.springframework.data.domain.Page;

import com.ftninformatika.jwd.test.model.Linija;

public interface LinijaService {
	
	Linija findOneById(Long id);

	Linija save(Linija linija);

	Linija update(Linija linija);

	Linija delete(Long id);

	Page<Linija> findAll(Integer pageNo);

	List<Linija> findAll();
	
	Linija oduzmiBrojMesta(Long id);
	
	Page<Linija> find(Double cenaKarteDo, String destinacija, Long prevoznik_id, Integer pageNo);
	
}
