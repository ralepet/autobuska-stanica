package com.ftninformatika.jwd.test.dto;

public class PorudzbinaDto {
	
	Long id;
	
	Long proizvodId;
	
	String proizvodNaziv;
	
	Integer kolicina;

	public PorudzbinaDto() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getProizvodId() {
		return proizvodId;
	}

	public void setProizvodId(Long proizvodId) {
		this.proizvodId = proizvodId;
	}

	public String getProizvodNaziv() {
		return proizvodNaziv;
	}

	public void setProizvodNaziv(String proizvodNaziv) {
		this.proizvodNaziv = proizvodNaziv;
	}

	public Integer getKolicina() {
		return kolicina;
	}

	public void setKolicina(Integer kolicina) {
		this.kolicina = kolicina;
	}
	
	

}
