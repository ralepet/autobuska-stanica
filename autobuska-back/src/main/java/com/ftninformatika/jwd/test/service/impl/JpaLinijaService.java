package com.ftninformatika.jwd.test.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.test.model.Linija;
import com.ftninformatika.jwd.test.repository.LinijaRepository;
import com.ftninformatika.jwd.test.service.LinijaService;

@Service
public class JpaLinijaService implements LinijaService{
	
	@Autowired
	private LinijaRepository linijaRepository;

	@Override
	public Linija findOneById(Long id) {
		return linijaRepository.findOneById(id);
	}

	@Override
	public Linija save(Linija linija) {
		return linijaRepository.save(linija);
	}

	@Override
	public Linija update(Linija linija) {
		return linijaRepository.save(linija);
	}

	@Override
	public Linija delete(Long id) {
		Optional<Linija> linija = linijaRepository.findById(id);
		if (linija.isPresent()) {
			linijaRepository.deleteById(id);
			return linija.get();
		}
		return null;

	}

	@Override
	public Page<Linija> findAll(Integer pageNo) {
		return linijaRepository.findAll(PageRequest.of(pageNo, 4));
	}

	@Override
	public List<Linija> findAll() {
		return linijaRepository.findAll();
	}

	@Override
	public Linija oduzmiBrojMesta(Long id) {
		
		Linija linija = linijaRepository.findOneById(id); // prvo nadjemo liniju
		
		linija.setBrojMesta(linija.getBrojMesta() - 1); //umanjujemo broj slobodnih mesta za 1
		
		return linijaRepository.save(linija); // cuvamo rezultat u bazi

	}
	
	@Override
    public Page<Linija> find(Double cenaKarteDo, String destinacija, Long prevoznik_id, Integer pageNo) {
    	
    	if (destinacija == null) {
    		destinacija = "";
        }

        if (cenaKarteDo == null) {
            cenaKarteDo = Double.MAX_VALUE;
        }
        
        if (prevoznik_id == null) {
        	return linijaRepository.findByDestinacijaIgnoreCaseContainsAndCenaLessThanEqual(
        			destinacija, cenaKarteDo, PageRequest.of(pageNo, 4));
        }

        return linijaRepository.findByDestinacijaIgnoreCaseContainsAndCenaLessThanEqualAndPrevoznikId(
    			destinacija, cenaKarteDo, prevoznik_id, PageRequest.of(pageNo, 4));
    }

}
