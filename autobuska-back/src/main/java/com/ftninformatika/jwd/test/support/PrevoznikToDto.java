package com.ftninformatika.jwd.test.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.PrevoznikDto;
import com.ftninformatika.jwd.test.model.Prevoznik;

@Component
public class PrevoznikToDto implements Converter<Prevoznik, PrevoznikDto>{

	@Override
	public PrevoznikDto convert(Prevoznik source) {
		// TODO Auto-generated method stub
		
		PrevoznikDto dto = new PrevoznikDto();
		dto.setId(source.getId());
		dto.setNaziv(source.getNaziv());
		dto.setAdresa(source.getAdresa());
		dto.setPib(source.getPib());
		return dto;
	}
	
	
	public List<PrevoznikDto> convert(List<Prevoznik>list){
		List<PrevoznikDto>dto = new ArrayList<PrevoznikDto>();
		for(Prevoznik p : list) { 
			dto.add(convert(p));
		}
		return dto;
	}
	

}
