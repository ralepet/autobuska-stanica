package com.ftninformatika.jwd.test.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.ftninformatika.jwd.test.model.Linija;

@Repository
public interface LinijaRepository extends JpaRepository<Linija, Long>{
	
	Linija findOneById(Long id);
	
	@Query("SELECT l FROM Linija l WHERE" +
            "(l.cena <= :cenaKarteDo) AND "+
            "(:destinacija = NULL OR l.destinacija LIKE :destinacija) AND " +
            "(:prevoznik_id = NULL OR l.prevoznik.id = :prevoznik_id) ")
    Page<Linija> search( @Param("cenaKarteDo") Double cenaKarteDo, @Param("destinacija") String destinacija,
    		@Param("prevoznik_id") Long prevoznik_id, Pageable pageable);
	
	 Page<Linija> findByDestinacijaIgnoreCaseContainsAndCenaLessThanEqualAndPrevoznikId( String destinacija, Double cenaKarteDo,  
	    			Long prevoznik_id, Pageable pageable);
	 
	 Page<Linija> findByDestinacijaIgnoreCaseContainsAndCenaLessThanEqual( String destinacija, Double cenaKarteDo,  
 			Pageable pageable);
	
	/*
	 *  Page<Film> findByNazivIgnoreCaseContainsAndTrajanjeBetweenAndZanroviId(String naziv, Integer trajanjeOd, Integer trajanjeDo, Long zanrId,
                                                                           Pageable pageable);

    Page<Film> findByNazivIgnoreCaseContainsAndTrajanjeBetween(String naziv,Integer trajanjeOd,Integer trajanjeDo,  Pageable pageable);
	 */

}