package com.ftninformatika.jwd.test.service;

import java.util.List;

import com.ftninformatika.jwd.test.model.Prevoznik;

public interface PrevoznikService {
	
	Prevoznik findOneById(Long id);

	List<Prevoznik> findAll();
	
	Prevoznik save(Prevoznik prevoznik);

}
