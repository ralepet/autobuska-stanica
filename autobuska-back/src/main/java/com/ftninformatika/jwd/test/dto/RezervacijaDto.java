package com.ftninformatika.jwd.test.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

public class RezervacijaDto {
	
	private Long id;
	
	@Positive(message = "Id mora biti pozitivan broj.")
	@NotNull
	private Long linijaId;

	public RezervacijaDto() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getLinijaId() {
		return linijaId;
	}

	public void setLinijaId(Long linijaId) {
		this.linijaId = linijaId;
	}

}
