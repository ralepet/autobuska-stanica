package com.ftninformatika.jwd.test.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ftninformatika.jwd.test.dto.LinijaDto;
import com.ftninformatika.jwd.test.model.Linija;
import com.ftninformatika.jwd.test.service.LinijaService;
import com.ftninformatika.jwd.test.support.DtoToLinija;
import com.ftninformatika.jwd.test.support.LinijaToDto;


@RestController
@RequestMapping(value = "/api/linije", produces = MediaType.APPLICATION_JSON_VALUE)
public class LinijaController {
	
	@Autowired
	private LinijaService linijaService;
	
	@Autowired
	private LinijaToDto toDto;
	
	@Autowired
	private DtoToLinija toLinija;
	
	/*
	@GetMapping
	public ResponseEntity<List<LinijaDto>> getAll(
			@RequestParam(value = "pageNo", defaultValue = "0") int pageNo) {
		
		// putanja http://localhost:8080/api/linije?pageNo=0 ili neki drugi broj umesto 0

		Page<Linija> page = linijaService.findAll(pageNo);

		HttpHeaders headers = new HttpHeaders();
		headers.add("Total-Pages", Integer.toString(page.getTotalPages()));

		return new ResponseEntity<>(toDto.convert(page.getContent()), HttpStatus.OK);
	}
	*/
	
	
	@GetMapping
	public ResponseEntity<List<LinijaDto>> getAll(
			@RequestParam(required=false) Double cenaKarteDo,
			@RequestParam(required=false) String destinacija,
			@RequestParam(required=false) Long prevoznik_id,
			@RequestParam(value = "pageNo", defaultValue = "0") int pageNo) {
		
		// putanja http://localhost:8080/api/linije?pageNo=0 ili neki drugi broj umesto 0
		
		//Page<Linija> page = linijaService.findAll(pageNo);

		Page<Linija> page = linijaService.find(cenaKarteDo, destinacija, prevoznik_id, pageNo);
		

		HttpHeaders headers = new HttpHeaders();
		headers.add("Total-Pages", Integer.toString(page.getTotalPages()));

		return new ResponseEntity<>(toDto.convert(page.getContent()), headers, HttpStatus.OK);
	}
	
	
	@GetMapping("/{id}")
    public ResponseEntity<LinijaDto> getOne(@PathVariable Long id){
        Linija linija = linijaService.findOneById(id);

        if(linija != null) {
            return new ResponseEntity<>(toDto.convert(linija), HttpStatus.OK);
        }else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
	
	//@PreAuthorize("hasAnyAuthority('ADMIN')")
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<LinijaDto> create(@Valid @RequestBody LinijaDto linijaDto) { 
		Linija linija = toLinija.convert(linijaDto); 
		
		if (linija.getPrevoznik() == null) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
 
		Linija sacuvanaLinija = linijaService.save(linija);

		return new ResponseEntity<>(toDto.convert(sacuvanaLinija), HttpStatus.CREATED);
	}
	
	//@PreAuthorize("hasAnyAuthority('ADMIN')")
	@PutMapping(value = "/{id}",consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<LinijaDto> update(@PathVariable Long id, @Valid @RequestBody LinijaDto linijaDto){ 

        if(!id.equals(linijaDto.getId())) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
 
        Linija linija = toLinija.convert(linijaDto); 
        Linija sacuvanaLinija = linijaService.update(linija);

        return new ResponseEntity<>(toDto.convert(sacuvanaLinija),HttpStatus.OK);
    }
	
	//@PreAuthorize("hasAnyAuthority('ADMIN')")
	@DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id){
        Linija obrisanaLinija = linijaService.delete(id); 

        if(obrisanaLinija != null) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
	
	@ExceptionHandler(value = DataIntegrityViolationException.class)
    public ResponseEntity<Void> handle() {
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }
}
