package com.ftninformatika.jwd.test.support;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.ftninformatika.jwd.test.dto.PrevoznikDto;
import com.ftninformatika.jwd.test.model.Prevoznik;
import com.ftninformatika.jwd.test.service.PrevoznikService;

@Component
public class DtoToPrevoznik  implements Converter<PrevoznikDto, Prevoznik> {
	
	@Autowired
	private PrevoznikService prevoznikService;

	@Override
	public Prevoznik convert(PrevoznikDto dto) { 
		Prevoznik entity;   
		if (dto.getId() == null) {
			entity = new Prevoznik();
		} else {
			entity = prevoznikService.findOneById(dto.getId());
		}
		
		entity.setNaziv(dto.getNaziv());
		entity.setAdresa(dto.getAdresa());
		entity.setPib(dto.getPib());

		return entity;
	}
	
	

}
