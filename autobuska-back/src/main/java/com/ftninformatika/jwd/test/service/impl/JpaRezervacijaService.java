package com.ftninformatika.jwd.test.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ftninformatika.jwd.test.model.Rezervacija;
import com.ftninformatika.jwd.test.repository.RezervacijaRepository;
import com.ftninformatika.jwd.test.service.RezervacijaService;

@Service
public class JpaRezervacijaService implements RezervacijaService{
	
	@Autowired
	private RezervacijaRepository rezervacijaRepository;

	@Override
	public Rezervacija findOneById(Long id) {
		return rezervacijaRepository.findOneById(id);
	}

	@Override
	public List<Rezervacija> findAll() {
		return rezervacijaRepository.findAll();
	}

	@Override
	public Rezervacija save(Rezervacija rezervacija) {
		return rezervacijaRepository.save(rezervacija);
	}

	@Override
	public Rezervacija update(Rezervacija rezervacija) {
		return rezervacijaRepository.save(rezervacija);
	}

}
