INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (1,'miroslav@maildrop.cc','miroslav','$2y$12$NH2KM2BJaBl.ik90Z1YqAOjoPgSd0ns/bF.7WedMxZ54OhWQNNnh6','Miroslav','Simic','ADMIN');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (2,'tamara@maildrop.cc','tamara','$2y$12$DRhCpltZygkA7EZ2WeWIbewWBjLE0KYiUO.tHDUaJNMpsHxXEw9Ky','Tamara','Milosavljevic','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (3,'petar@maildrop.cc','petar','$2y$12$i6/mU4w0HhG8RQRXHjNCa.tG2OwGSVXb0GYUnf8MZUdeadE4voHbC','Petar','Jovic','KORISNIK');
INSERT INTO korisnik (id, e_mail, korisnicko_ime, lozinka, ime, prezime, uloga)
              VALUES (4,'mare@gmail.com','mare','$2a$10$hW5o6Trv06hWFfqfm2zN9.foYgCIcxwVl7wGWUo0QMdiCLOsIpnk2','Marko','Markovic','ADMIN');
              
INSERT INTO prevoznik (id, naziv, pib, adresa) VALUES (1,'Subotica trans','111','Genericka adresa 1 Subotica');             
INSERT INTO prevoznik (id, naziv, pib, adresa) VALUES (2,'Lasta','112','Genericka adresa 1 Beograd');
INSERT INTO prevoznik (id, naziv, pib, adresa) VALUES (3,'Nis ekspres','113','Genericka adresa 1 Nis');

INSERT INTO linija (id, broj_mesta, cena, destinacija, vreme_polaska, prevoznik_id) VALUES (1, 2, 500.0,'Beograd','10:00', 1);
INSERT INTO linija (id, broj_mesta, cena, destinacija, vreme_polaska, prevoznik_id) VALUES (2, 50, 600.0,'Novi Sad','11:00', 2);
INSERT INTO linija (id, broj_mesta, cena, destinacija, vreme_polaska, prevoznik_id) VALUES (3, 42, 1200.0,'Zrenjanin','09:00', 2);
INSERT INTO linija (id, broj_mesta, cena, destinacija, vreme_polaska, prevoznik_id) VALUES (4, 45, 1300.0,'Nis','10:00', 3);
INSERT INTO linija (id, broj_mesta, cena, destinacija, vreme_polaska, prevoznik_id) VALUES (5, 40, 550.0,'Loznica','12:00', 1);
INSERT INTO linija (id, broj_mesta, cena, destinacija, vreme_polaska, prevoznik_id) VALUES (6, 30, 630.0,'Sid','15:00', 3);
INSERT INTO linija (id, broj_mesta, cena, destinacija, vreme_polaska, prevoznik_id) VALUES (7, 3, 700.0,'Sid','15:00', 2);

